#!/usr/bin/perl

use strict;
use warnings;

use WebService::Validator::HTML::W3C;
use FreezeThaw qw(freeze thaw);

use SysUtils;

my $VERSION = '1.0.0';

my $CHECKLINK_CMD = 'checklink -b %b -r -s -X "^mailto:|^http://www.w3.org/TR%x|.css(\?.*)?|.js(\?.*)?$" > %o';
my $SHOW_LINK_ISSUES = 2;
my $SHOW_HTML_ISSUES = 1;

my $HELP = 0;
my $BY_CODE = 0;
my $FILTER = undef;
my $CHECKLINK_FILE = undef;
my $INFILE = undef;
my $BY_LINE = 0;
my $LINKS_ONLY = 0;
my $OUTFILE = 'checkSite.log';
my $REPORT_FILE = undef;
my $NO_REDIRECTS = 0;
my $SITEMAP = undef;
my $SITEMAP_CHANGE_FREQ = 'daily';
my $VALIDATOR_URL = 'http://localhost/w3c-validator/check';
my $VERBOSE = 0;
my $WARNINGS = 0;
my $EXCLUDE = undef;
(my $opts, my $args, my $err) = &SysUtils::parseOptions({
  'help|?' => \$HELP,
  'by-code|c' => \$BY_CODE,
  'filter|f=' => \$FILTER,
  'checklink|h=' => \$CHECKLINK_FILE,
  'infile|i=' => \$INFILE,
  'by-line|l' => \$BY_LINE,
  'links-only|L' => \$LINKS_ONLY,
  'outfile|o=' => \$OUTFILE,
  'report|r=' => \$REPORT_FILE,
  'no-redirects|R' => \$NO_REDIRECTS,
  'sitemap|s=' => \$SITEMAP,
  'changefreq|S=' => \$SITEMAP_CHANGE_FREQ,
  'validator-url|u=' => \$VALIDATOR_URL,
  'warnings|w' => \$WARNINGS,
  'verbose|v' => \$VERBOSE,
  'exclude|x=' => \$EXCLUDE
}, 1);

if ($HELP) {
  &printUsage(0);
}
if ($err) {
  print STDERR ($err);
  &printUsage(1);
}

my $BASE_URL = $args->[0];
my $n = 0;
foreach my $item ($BASE_URL, $CHECKLINK_FILE, $INFILE) {
  if (defined($item)) {
    $n++;
  }
}
if ($n != 1) {
  print STDERR "Exactly one of the following items must be specified:\n";
  print STDERR "- a url (as last command-line argument)\n";
  print STDERR "- a checklink result file (-h option)\n";
  print STDERR "- a checkSite log file (-i option)\n";
  &printUsage(1);
}


#--------
# my $validator = WebService::Validator::HTML::W3C->new(
#     validator_uri => $VALIDATOR_URL,
#     detailed    =>  1
# );
# if ($validator->validate("http://www.islas-canarias-reisen.de/madeira/ferienhaus_madeira_3.php")) {
#   foreach my $err (@{$validator->errors()}) {
#     print($err->msg, "\n");
#     &SysUtils::printRefDump($err);
#   }
# }
# exit;
#--------



my $PAGES;
if ($INFILE) {
  $PAGES = &readLog($INFILE);
}
else {
  unless ($CHECKLINK_FILE) {
    $CHECKLINK_FILE = 'checklink.log';
    &callChecklink($BASE_URL, $CHECKLINK_FILE, $EXCLUDE);
  }
  $PAGES = &parseChecklinkFile($CHECKLINK_FILE);
  unless ($LINKS_ONLY) {
    &validatePages($PAGES);
  }
  &saveLog($PAGES, $OUTFILE);
}

my $REPORT = &buildReport($PAGES);

my $OUT = *STDOUT;
if ($REPORT_FILE) {
  unless (open($OUT, ">$REPORT_FILE")) {
    die ("Couldn't open '$REPORT_FILE' for writing!\n");
  }
}
print $OUT (${$REPORT});
if ($REPORT_FILE) {
  close($OUT);
}

if ($SITEMAP) {
  &writeSitemap($PAGES);
}

#======================================================================


sub callChecklink {
  my $BASE_URL = shift;
  my $outfile = shift;
  my $exclude = shift;

  my $cmd = $CHECKLINK_CMD;
  $cmd =~ s/%b/$BASE_URL/g;
  $cmd =~ s/%o/$outfile/g;
  my $x = ($exclude ? "|$exclude" : "");
  $cmd =~ s/%x/$x/g;
  &report("Calling checklink:\n  $cmd\n  Please note: This step may take a looong time to complete...\n");
  system($cmd);
}


sub parseChecklinkFile {
  my $path = shift;
  &report("Parsing checklink result '$path'...\n");
  my $pages = {};
  (my $content, my $err) = &SysUtils::readFromFile($path);
  if ($err) {
    die($err);
  }
  my @pages = split(/-{40,}\n/, ${$content});
  foreach my $page (@pages) {
    if ($page =~ m/^Processing\t(.+)$/m) {
      my $url = $1;
      &report("  Parsing results for page $url...\n");
      $pages->{$url} = {};

      my $marker = '----------------------------------------';
      my @hrefs = ();
      while ($page =~ s/^(\w+:\S+)\s*\n/$marker/m) {
        my $href = $1;
        push(@hrefs, $href);
      }
      my @parts = split(/$marker/, $page);
      shift(@parts);

      for (my $i = 0; $i < scalar(@hrefs); $i++) {
        my $href = $hrefs[$i];
        my $part = $parts[$i];
        if ($part =~ /^\s*(->\s*(\w+:\S+)\s*\n\s*)?Lines?:\s*(.+)\n\s*Code:\s*([^\n]+)(\n.+The following fragments need to be fixed:\n(.+))?/sm) {
          my $redirect = $2;
          my $lines = $3;
          my $code = $4;
          my $fragments = $6;
          unless ($code =~ /^\(N\/A\)/) {
            &report("    Found $href\n");
            if ($fragments) {
              my @fragments = split(/\n/, $fragments);
              my %anchors = ();
              foreach my $fragment (@fragments) {
                if ($fragment =~ m/^\s*(\S+)\s*Lines?:\s*(.+)/) {
                  my $anchor = $1;
                  my $lines = $2;
                  my @lines = split(/,\s*/, $lines);
                  unless (defined($anchors{$anchor})) {
                    $anchors{$anchor} = [];
                  }
                  push(@{$anchors{$anchor}}, @lines);
                }
              }
              foreach my $anchor (keys(%anchors)) {
                my $aHref = "$href#$anchor";
                my $lines = join(', ', @{$anchors{$anchor}});
                $pages->{$url}->{links}->{codes}->{$code}->{$aHref} = {
                  lines => $lines,
                  fragment => $anchor
                };
                $pages->{$url}->{links}->{hrefs}->{$aHref} = {
                  code => $code,
                  lines => $lines,
                  fragment => $anchor
                };
                foreach my $line (@{$anchors{$anchor}}) {
                  $pages->{$url}->{lines}->{$line}->{links}->{$aHref} = {
                    code => $code,
                    fragment => $anchor
                  };
                }
              }
            }
            else {
              $pages->{$url}->{links}->{codes}->{$code}->{$href} = {
                lines => $lines,
                redirect => $redirect
              };
              if ($redirect) {
                $pages->{$url}->{links}->{has_redirects} = 1;
              }
              else {
                $pages->{$url}->{links}->{has_non_redirects} = 1;
              }
              $pages->{$url}->{links}->{hrefs}->{$href} = {
                code => $code,
                lines => $lines,
                redirect => $redirect
              };
              my @lines = split(/,\s*/, $lines);
              foreach my $line (@lines) {
                $pages->{$url}->{lines}->{$line}->{links}->{$href} = {
                  code => $code,
                  redirect => $redirect
                };
              }
            }
          }
        }
      }
    }
  }
  return $pages;
}


sub validatePages {
  &report("Validating pages...\n");
  my $pages = shift;
  my $validator = WebService::Validator::HTML::W3C->new(
    validator_uri => $VALIDATOR_URL,
    detailed    =>  1
  );
  foreach my $url (sort(keys(%{$pages}))) {
    &report("  Validating page '$url'...\n");
    eval {
      if ($validator->validate($url)) {
        if ($validator->is_valid()) {
          $pages->{$url}->{html}->{status}->{valid} = 1;
        }
        else {
          $pages->{$url}->{html}->{status}->{valid} = 0;
          foreach my $err (@{$validator->errors()}) {
            my $line = $err->line() ? $err->line() : 0;
            unless (defined($pages->{$url}->{html}->{errors}->{lines}->{$line})) {
              $pages->{$url}->{html}->{errors}->{lines}->{$line} = [];
            }
            push(@{$pages->{$url}->{html}->{errors}->{lines}->{$line}}, $err->msg());
            unless (defined($pages->{$url}->{lines}->{$line}->{html}->{errors})) {
              $pages->{$url}->{lines}->{$line}->{html}->{errors} = [];
            }
            push(@{$pages->{$url}->{lines}->{$line}->{html}->{errors}}, $err->msg());
          }
        }
        $pages->{$url}->{html}->{status}->{warnings} = 0;
        foreach my $warn (@{$validator->warnings()}) {
          my $line = $warn->line() ? $warn->line() : 0;
          $pages->{$url}->{html}->{status}->{warnings} = 1;
          unless (defined($pages->{$url}->{html}->{warnings}->{lines}->{$line})) {
            $pages->{$url}->{html}->{warnings}->{lines}->{$line} = [];
          }
          push(@{$pages->{$url}->{html}->{warnings}->{lines}->{$line}}, $warn->msg());
          unless (defined($pages->{$url}->{lines}->{$line}->{html}->{warnings})) {
            $pages->{$url}->{lines}->{$line}->{html}->{warnings} = [];
          }
          push(@{$pages->{$url}->{lines}->{$line}->{html}->{warnings}}, $warn->msg());
        }

      }
      else {
        $pages->{$url}->{html}->{status}->{error} = $validator->validator_error();
      }
      1;
    } or do {
      $pages->{$url}->{html}->{status}->{error} = $@;
    }
  }
}


sub readLog {
  my $path = shift;
  (my $dump, my $err) = &SysUtils::readFromFile($path);
  if ($err) {
    die($err);
  }
  (my $pages) = thaw(${$dump});
  return $pages;
}

sub saveLog {
  my $pages = shift;
  my $path = shift;
  my $dump = freeze($pages);
  my $err = &SysUtils::writeToFile($path, $dump);
  if ($err) {
    die($err);
  }
}


sub buildReport {
  my $pages = shift;
  my $report = '';
  foreach my $url (sort(keys(%{$pages}))) {
    my $page = $pages->{$url};

    my $linkStatus = &getLinkStatus($page);
    my $htmlStatus = &getHtmlStatus($page);
    my $hasLinkIssues = ($linkStatus eq 'ok' || ($linkStatus eq 'ok, but with redirects' && $NO_REDIRECTS)) ? 0 : 1;
    my $hasHtmlIssues = ($htmlStatus eq 'ok' || ($htmlStatus eq 'ok, but with warnings' && !$WARNINGS)) ? 0 : 2;
    my $hasIssues = $hasLinkIssues | $hasHtmlIssues;
    my $includeUrl = !$FILTER || ($hasIssues & $FILTER);
    my $showLinkStatus = !$FILTER || $FILTER & 1;
    my $showHtmlStatus = !$FILTER || $FILTER & 2;

#     print("$hasLinkIssues - $hasHtmlIssues - $hasIssues - $FILTER - $includeUrl\n");
    if ($includeUrl) {

      $report .= "URL: $url\n";

      if ($BY_LINE) {

        if ($showHtmlStatus) {
          $report .= "  HTML: $htmlStatus";
          if ($page->{html}->{status}->{error}) {
            $report .= $page->{html}->{status}->{error};
          }
          $report .= "\n";
        }
        if ($showLinkStatus) {
          $report .= "  LINKS: $linkStatus\n";
        }
        foreach my $line (sort {$a <=> $b} (keys(%{$page->{lines}}))) {
          if ($showHtmlStatus) {
            foreach my $msg (@{$page->{lines}->{$line}->{html}->{errors}}) {
              $report .= "  LINE $line: HTML ERROR: $msg\n";
            }
            if ($WARNINGS && $page->{lines}->{$line}->{html}->{warnings}) {
              foreach my $msg (@{$page->{lines}->{$line}->{html}->{warnings}}) {
                $report .= "  LINE $line: HTML WARNING: $msg\n";
              }
            }
          }
          if ($showLinkStatus) {
            foreach my $href (sort(keys(%{$page->{lines}->{$line}->{links}}))) {
              unless ($NO_REDIRECTS && $page->{lines}->{$line}->{links}->{$href}->{redirect}) {
                $report .= "  LINE $line: $href: " . $page->{lines}->{$line}->{links}->{$href}->{code};
                if ($page->{lines}->{$line}->{links}->{$href}->{redirect}) {
                  $report .= ' (' . $page->{lines}->{$line}->{links}->{$href}->{redirect} . ')';
                }
                if ($page->{lines}->{$line}->{links}->{$href}->{fragment}) {
                  $report .= ' (undefined fragment ' . $page->{lines}->{$line}->{links}->{$href}->{fragment} . ')';
                }
                $report .= "\n";
              }
            }
          }
        }

      }
      else {

        if ($showHtmlStatus) {
          $report .= "  HTML: $htmlStatus";
          if ($page->{html}->{status}->{error}) {
            $report .= $page->{html}->{status}->{error};
          }
          $report .= "\n";
          if ($page->{html}->{errors}) {
            $report .= "    ERRORS:\n";
            foreach my $line (sort {$a <=> $b} (keys(%{$page->{html}->{errors}->{lines}}))) {
              foreach my $msg (@{$page->{html}->{errors}->{lines}->{$line}}) {
                $report .= "      LINE $line: $msg\n";
              }
            }
          }
          if ($WARNINGS && $page->{html}->{warnings}) {
            $report .= "    WARNINGS:\n";
            foreach my $line (sort {$a <=> $b} (keys(%{$page->{html}->{warnings}->{lines}}))) {
              foreach my $msg (@{$page->{html}->{warnings}->{lines}->{$line}}) {
                $report .= "      LINE $line: $msg\n";
              }
            }
          }
        }
        if ($showLinkStatus) {
          $report .= "  LINKS: $linkStatus\n";

          if ($BY_CODE) {
            foreach my $code (sort(keys(%{$page->{links}->{codes}}))) {
              my $subReport = '';
              foreach my $href (sort(keys(%{$page->{links}->{codes}->{$code}}))) {
                unless ($NO_REDIRECTS && $page->{links}->{codes}->{$code}->{$href}->{redirect}) {
                  $subReport .= "      $href";
                  if ($page->{links}->{codes}->{$code}->{$href}->{redirect}) {
                    $subReport .= ' (' . $page->{links}->{codes}->{$code}->{$href}->{redirect} . ')';
                  }
                  if ($page->{links}->{codes}->{$code}->{$href}->{fragment}) {
                    $subReport .= ' (undefined fragment ' . $page->{links}->{codes}->{$code}->{$href}->{fragment} . ')';
                  }
                  $subReport .= '; LINE(S): ' . $page->{links}->{codes}->{$code}->{$href}->{lines} . "\n";
                }
              }
              if ($subReport) {
                $report .= "    CODE: $code\n$subReport";
              }
            }
          }
          else {
            foreach my $href (sort(keys(%{$page->{links}->{hrefs}}))) {
              unless ($NO_REDIRECTS && $page->{links}->{hrefs}->{$href}->{redirect}) {
                $report .= "    $href - CODE: ";
                $report .= $page->{links}->{hrefs}->{$href}->{code};
                if ($page->{links}->{hrefs}->{$href}->{redirect}) {
                  $report .= ' (' . $page->{links}->{hrefs}->{$href}->{redirect} . ')';
                }
                if ($page->{links}->{hrefs}->{$href}->{fragment}) {
                  $report .= ' (undefined fragment ' . $page->{links}->{hrefs}->{$href}->{fragment} . ')';
                }
                $report .= '; LINE(S): ' . $page->{links}->{hrefs}->{$href}->{lines} . "\n";
              }
            }
          }
        }

      }
    }

  }
  return \$report;
}

sub getHtmlStatus {
  my $page = shift;
  my $status = 'unknown';
  if (defined($page->{html}->{status}->{valid})) {
    if ($page->{html}->{status}->{valid}) {
      if (!$WARNINGS && $page->{html}->{status}->{warnings}) {
        $status = 'ok, but with warnings';
      }
      else {
        $status = 'ok';
      }
    }
    else {
      $status = 'issues found';
    }
  }
  elsif (defined($page->{html}->{status}->{error})) {
    $status = $page->{html}->{status}->{error};
  }
  return $status;
}

sub getLinkStatus {
  my $page = shift;
  my $status = 'ok';
  if (defined($page->{links})) {
    if (defined($page->{links}->{has_non_redirects})) {
      $status = 'issues found';
    }
    else {
      if ($NO_REDIRECTS) {
        $status = defined($page->{links}->{has_redirects}) ? 'ok, but with redirects' : 'ok';
      }
      else {
        $status = defined($page->{links}->{has_redirects}) ? 'issues found' : 'ok';
      }
    }
  }
  if ($status eq 'unknown') {
    &SysUtils::printRefDump($page);exit;
  }
  return $status;
}

sub writeSitemap {
  my $pages = shift;
  
  my $map = <<EOT;
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
EOT

  foreach my $url (sort(keys(%{$pages}))) {
    $map .= "  <url>\n";
    $map .= "    <loc>$url</loc>\n";
    if ($SITEMAP_CHANGE_FREQ) {
      $map .= "    <changefreq>$SITEMAP_CHANGE_FREQ</changefreq>\n";
    }
    $map .= "  </url>\n";
  }
  $map .= "</urlset>\n";
  print $map;
  my $err = SysUtils::writeToFile($SITEMAP, \$map);
  if ($err) {
    die($err);
  }
}


sub report {
  if ($VERBOSE) {
    print(@_);
  }
}

sub printUsage {
  print <<EOT;

checkSite.pl -- Check websites for broken links and html syntax errors.
Version $VERSION
-----------------------------------------------------------------------
Usage: checkSite.pl [options] (-i <infile> | <url>)
with possible options:
-?|--help             : Show this message.
-c|--by-code          : Output link issues sorted by HTTP status code,
                        rather than by href.
-f|--filter <code>    : Only report pages with link (code = 1) or html
                        (code = 2) or both kinds (code = 3) of issues.
-h|--checklink <path> : Use checklink result file <path> rather than
                        calling checklink for a given <url>.
-i|--infile <path>    : Use a checkSite log file rather than calling
                        checklink and the W3C validator; useful for
                        re-formatting the same page's issues with
                        different checkSite.pl output options.
-l|--by-line          : Output results sorted by line number, rather
                        than by issue type.
-L|--links-only       : Don't validate, just process checklink result.
-o|--outfile <path>   : Write checkSite log to <path> rathern than the
                        default location. [$OUTFILE]
-r|--report <path>    : Output result to <path> rather than STDOUT.
-R|--no-redirects     : Ommit successful redirects from report.
-s|--sitemap <path>   : From the urls processed by checklink, create
                        a sitemap file at <path>.
-S|--changefreq <str> : Chenge frequency the sitemap entries will be
                        attributed with. [$SITEMAP_CHANGE_FREQ]
-u|--validator        : URL of the W3C validator to use.
                        [$VALIDATOR_URL]
-v|--verbose          : Talk a lot.
-w|--warnings         : Include W3C validator warnings into output.
-x|--exclude          : Regex pattern to be added to checklink's
                        exclude option.
<url>                 : URL of website to be checked (excludes -i)
Requires w3c checklink to be installed and in PATH as well as
SysUtils.pm and WebService::Validator::HTML::W3C to be installed an
in PERL5LIB.
EOT

  if (@_) {
    exit(shift);
  }

}
